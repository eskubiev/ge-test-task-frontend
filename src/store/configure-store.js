import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import rootReducer from './root-reducer';
import { loadState } from '../lib/local-storage';

const configureStore = () => {
    const persistedState = loadState();
    const middleware = composeWithDevTools(applyMiddleware(thunk));
    const store = createStore(rootReducer, persistedState, middleware);

    if (process.env.NODE_ENV !== 'production' && module.hot) {
        module.hot.accept('./root-reducer', () =>
            store.replaceReducer(rootReducer)
        );
    }

    return store;
};

export default configureStore;
