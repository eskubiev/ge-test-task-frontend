import { combineReducers } from 'redux';

import profile from '../components/profile/reducer';
import forms from '../components/forms/reducer';

const reducers = {
    profile,
    forms
};

const rootReducer = combineReducers(reducers);

export default rootReducer;
