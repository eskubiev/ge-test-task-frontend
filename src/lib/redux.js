import { fakeFetch } from '../api-emulation/fake-fetch';
import isFunction from 'lodash/isFunction';

export const makeSimpleApiRequest = async (
    dispatch,
    url,
    fetchOptions,
    actionName,
    onSuccess,
    onError
) => {
    dispatch({ type: `${actionName}_REQUEST` });
    try {
        const response = await fakeFetch(url, fetchOptions);
        dispatch({ type: `${actionName}_SUCCESS`, response });
        if (isFunction(onSuccess)) {
            onSuccess(response);
        }
    } catch (error) {
        console.log(error);
        dispatch({ type: `${actionName}_FAILURE`, error });
        if (isFunction(onError)) {
            onError(error);
        }
    }
};
