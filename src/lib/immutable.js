import { Map, List, fromJS } from 'immutable';

export const convertListToMap = (list, keyId) => {
    if (!Array.isArray(list) || list.length === 0) {
        return Map();
    }

    if (!list[0][keyId]) {
        return List(fromJS(list)).toMap();
    }

    let result = Map();

    list.forEach(item => {
        result = result.set(item[keyId], fromJS(item));
    });

    return result;
};
