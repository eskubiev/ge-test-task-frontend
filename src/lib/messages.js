const validationPresence = 'Поле не может быть пустым';
const notSaved = 'Изменения не сохранены';

export default {
    validation: {
        presence: validationPresence,
        presenceNotSaved: `${validationPresence}. ${notSaved}`,
        uniqueness: 'Не должно повторяться',
        requiredField: 'Это поле необходимо заполнить'
    },
    notSaved
};
