export const generateUniqueString = (otherStrings, prefix = '') => {
    const string = prefix;
    if (!Array.isArray(otherStrings)) {
        return string;
    }
    let index = 1;
    let testString = string;
    while (otherStrings.includes(testString)) {
        testString = `${string} ${index}`;
        index++;
    }
    return testString;
};
