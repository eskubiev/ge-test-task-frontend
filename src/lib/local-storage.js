import { fromJS } from 'immutable';

export const loadFromLocalStorage = key => {
    try {
        const serializedData = localStorage.getItem(key);
        if (serializedData === null) {
            return undefined;
        }
        const data = JSON.parse(serializedData);
        return data;
    } catch (e) {
        return undefined;
    }
};

export const saveToLocalStorage = (key, data) => {
    try {
        const serializedData = JSON.stringify(data);
        localStorage.setItem(key, serializedData);
    } catch (e) {}
};

export const loadState = () => {
    const state = loadFromLocalStorage('state');
    if (state === undefined) {
        return undefined;
    }
    let immutableState = {};
    Object.entries(state).forEach(([key, value]) => {
        immutableState[key] = fromJS(value);
    });
    return immutableState;
};

export const saveState = state => saveToLocalStorage('state', state);
