import messages from './messages';

export const getValidationErrors = (
    validationErrors = {},
    name,
    noSaveMessage
) => {
    const errors = validationErrors[name];
    if (!Array.isArray(errors)) {
        return '';
    }
    if (!noSaveMessage && !errors.includes(messages.notSaved)) {
        errors.push(messages.notSaved);
    }
    return validationErrors[name].join('. ');
};

export const uniquenessValidator = (value, options) => {
    if (
        Array.isArray(options.compareWith) &&
        options.compareWith.includes(value)
    ) {
        return options.message || messages.validation.uniqueness;
    }
    return;
};
