import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router-dom';

import { Paper, Toolbar, Button } from 'react-md';
import { Form } from '../forms';
import { ClientHome } from './';

import { page404Path, go } from '../app/routes';
import { signOut } from '../profile/actions';

import './styles.scss';

class ClientApp extends Component {
    render() {
        const {
            match: { path }
        } = this.props;
        return (
            <Fragment>
                <Toolbar
                    colored
                    title={
                        <span className="clickable" onClick={go.toClientApp}>
                            Привет, Пользователь!
                        </span>
                    }
                    className="toolbar"
                    actions={
                        <Button
                            icon
                            disabled={this.props.profile.get('pending')}
                            tooltipLabel="Выйти из системы"
                            tooltipPosition="left"
                            onClick={this.props.signOut}>
                            exit_to_app
                        </Button>
                    }
                />
                <div className="containerWrapper">
                    <Paper zDepth={1} className="container">
                        <Switch>
                            <Route
                                exact
                                path={path}
                                render={() => <ClientHome {...this.props} />}
                            />
                            <Route
                                path={`${path}form/:formId`}
                                component={Form}
                            />
                            <Redirect exact from={`${path}form`} to={path} />
                            <Redirect to={page404Path} />
                        </Switch>
                    </Paper>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = ({ profile }) => ({
    profile
});

const mapDispatchToProps = {
    signOut
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ClientApp);
