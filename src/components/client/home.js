import React from 'react';
import { Redirect } from 'react-router-dom';
import './styles.scss';

export default ({profile}) => {
    const primaryFormId = profile.get('primaryForm');
    if (primaryFormId) {
        return <Redirect to={`/form/${primaryFormId}`} />;
    }
    return (
        <div className="welcomeWrapper">
            <h2>Добро пожаловать!</h2>
            <h4>Для вас пока нет опросов...</h4>
        </div>
    );
};
