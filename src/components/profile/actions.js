import { makeSimpleApiRequest } from '../../lib/redux';
import { go } from '../app/routes';

export const SIGN_UP_REQUEST = 'SIGN_UP_REQUEST';
export const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS';
export const SIGN_UP_FAILURE = 'SIGN_UP_FAILURE';

export const SIGN_IN_REQUEST = 'SIGN_IN_REQUEST';
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
export const SIGN_IN_FAILURE = 'SIGN_IN_FAILURE';

export const SIGN_OUT = 'SIGN_OUT';

export const signUp = () => dispatch =>
    makeSimpleApiRequest(
        dispatch,
        '/api/user/signup',
        { method: 'POST' },
        'SIGN_UP',
        go.toClientApp
    );

export const signIn = () => dispatch =>
    makeSimpleApiRequest(
        dispatch,
        '/api/user',
        { method: 'POST' },
        'SIGN_IN',
        go.toAdminApp
    );

export const signOut = () => dispatch => {
    dispatch({ type: SIGN_OUT });
    go.toSignIn();
};
