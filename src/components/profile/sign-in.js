import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { Card, CardTitle, CardText, Button } from 'react-md';
import { PendingOverlay } from '../progress-indicators';

import { signIn, signUp } from './actions';
import './styles.scss';

class SignIn extends Component {
    redirectToHomeApp = role => {
        switch (this.props.profile.get('role')) {
            case 'client':
                return <Redirect to="/" />;
            case 'admin':
                return <Redirect to="/admin" />;
            default:
                return null;
        }
    };

    render() {
        return (
            <div className="signInWrapper">
                {this.redirectToHomeApp()}
                <Card className="card">
                    <CardTitle className="title" title="Войти в систему" />
                    <CardText className="buttonsWrapper">
                        <Button raised primary onClick={this.props.signUp}>
                            Зарегистрироваться как пользователь
                        </Button>
                        <Button raised primary onClick={this.props.signIn}>
                            Войти как администратор
                        </Button>
                        <p>
                            Работа с API эмулируется. Все данные сохраняются
                            локально в браузере.
                        </p>
                    </CardText>
                </Card>
                {this.props.profile.get('pending') && <PendingOverlay />}
            </div>
        );
    }
}

const mapStateToProps = ({ profile }) => ({
    profile
});

const mapDispatchToProps = {
    signIn,
    signUp
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SignIn);
