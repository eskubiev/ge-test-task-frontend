import * as Actions from './actions';
import { Map } from 'immutable';

const initialState = Map({
    pending: false,
    role: undefined
});

export default (state = initialState, action) => {
    switch (action.type) {
        case Actions.SIGN_UP_SUCCESS:
        case Actions.SIGN_IN_SUCCESS:
            return Map({ ...action.response }).set('pending', false);
        case Actions.SIGN_UP_REQUEST:
        case Actions.SIGN_IN_REQUEST:
            return state.set('pending', true);
        case Actions.SIGN_UP_FAILURE:
        case Actions.SIGN_IN_FAILURE:
            return state.set('pengin', false);
        case Actions.SIGN_OUT:
            return initialState;
        default:
            return state;
    }
};
