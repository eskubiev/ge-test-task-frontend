import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

export default (ComposedComponent, authorizedRoles) => {
    class RequireAuthentication extends Component {
        render() {
            const role = this.props.profile.get('role');
            if (role && authorizedRoles.includes(role)) {
                return <ComposedComponent {...this.props} />;
            }
            return <Redirect to="sign-in" />;
        }
    }

    const mapStateToProps = ({ profile }) => ({
        profile
    });

    return connect(mapStateToProps)(RequireAuthentication);
};
