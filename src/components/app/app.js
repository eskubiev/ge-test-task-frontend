import React, { Component } from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { connect } from 'react-redux';

import { SignIn, requireAuthentication } from '../profile';
import { Client } from '../client';
import { Admin } from '../admin';
import { Page404 } from '../utils';

export const browserHistory = createBrowserHistory();

class App extends Component {
    render() {
        return (
            <Router history={browserHistory}>
                <Switch>
                    <Route
                        path="/admin"
                        component={requireAuthentication(Admin, ['admin'])}
                    />
                    <Route path="/sign-in" component={SignIn} />
                    <Route
                        path="/"
                        component={requireAuthentication(Client, [
                            'client',
                            'admin'
                        ])}
                    />
                    <Route component={Page404} />
                </Switch>
            </Router>
        );
    }
}

const mapStateToProps = ({ profile }) => ({
    profile
});

export default connect(mapStateToProps)(App);
