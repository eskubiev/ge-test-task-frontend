import { browserHistory } from './app';

export const page404Path = '/error-404-not-found';

export const go = {
    toClientApp: () => browserHistory.push('/'),
    toAdminApp: () => browserHistory.push('/admin'),
    toFormEdit: id => browserHistory.push(`/admin/form/${id}`),
    toFormPreview: id => browserHistory.push(`/admin/form/${id}/preview`),
    toForm: id => browserHistory.push(`/form/${id}`),
    toSignIn: () => browserHistory.push('/sign-in'),
    toPage404: () => browserHistory.push(page404Path)
};
