import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import validate from 'validate.js';

import { Button } from 'react-md';
import { Field } from '../fields';
import { PendingOverlay } from '../progress-indicators';

import { getForm } from './actions';

import './styles.scss';
import { getValidationErrors } from '../../lib/validation';
import messages from '../../lib/messages';

class Form extends Component {
    state = { fieldsValues: {}, validationErrors: {} };

    componentDidMount() {
        const form = this.props.forms
            .get('items')
            .find(form => form.get('id') === this.props.match.params.formId);
        if (!form) {
            this.props.getForm(this.props.match.params.formId);
        }
    }

    change = (value, index) => {
        this.setState({
            fieldsValues: { ...this.state.fieldsValues, [index]: value }
        });
    };

    submit = fields => {
        const constraints = {};
        const result = fields.map((field, index) => {
            if (field.required) {
                constraints[index] = {
                    presence: { message: messages.validation.requiredField }
                };
            }
            return `${index + 1}. ${field.name} - ${
                this.state.fieldsValues[index]
            }`;
        });
        const validationErrors = validate(
            this.state.fieldsValues,
            constraints,
            { fullMessages: false }
        );
        if (validationErrors && Object.keys(validationErrors).length > 0) {
            this.setState({ validationErrors });
        } else {
            alert(`Ваши ответы:\n${result.join('\n')}`);
        }
    };

    renderFields = fields =>
        fields.map((field, index) => (
            <Field
                key={index}
                index={index}
                field={field}
                value={this.state.fieldsValues[index]}
                onChange={value => this.change(value, index)}
                error={Boolean(this.state.validationErrors[index])}
                errorText={getValidationErrors(
                    this.state.validationErrors,
                    index,
                    true
                )}
            />
        ));

    render() {
        const form = this.props.forms.getIn([
            'items',
            this.props.match.params.formId
        ]);
        if (!Map.isMap(form)) {
            return (
                <div className="formsViewsHeader">
                    <h3>
                        {this.props.forms.get('pending')
                            ? 'Подождите немного...'
                            : 'Форма не найдена'}
                    </h3>
                </div>
            );
        }
        const fields = form.get('fields').toJS();
        return (
            <div>
                <div className="formsViewsHeader">
                    <h3>{form.get('name')}</h3>
                </div>
                <div className="formFieldsWrapper">
                    {this.renderFields(fields)}
                </div>
                {this.props.forms.get('pending') && <PendingOverlay />}
                <div className="flexCenteredContent submitButtonWrapper">
                    <Button raised primary onClick={() => this.submit(fields)}>
                        Отправить
                    </Button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ forms }) => ({
    forms
});

const mapDispatchToProps = {
    getForm
};

Form.propTypes = {
    forms: PropTypes.instanceOf(Map).isRequired,
    getForm: PropTypes.func.isRequired
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Form);
