import React, { Fragment } from 'react';

import { Button } from 'react-md';
import { Form } from './';

import { go } from '../app/routes';
import './styles.scss';

export default props => (
    <Fragment>
        <div className="formsViewsHeader formPreviewControls">
            <Button
                raised
                primary
                onClick={() => go.toFormEdit(props.match.params.formId)}>
                Редактирование
            </Button>
        </div>
        <Form {...props} />
    </Fragment>
);
