import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';

import { Button } from 'react-md';
import { EditableField } from '../fields';

import './styles.scss';
import { generateUniqueString } from '../../lib/text';

class FormFields extends Component {
    handleFieldChange = (field, index) => {
        const form = this.props.form.toJS();
        form.fields[index] = field;
        this.props.saveForm(form.id, form);
    };

    changeFieldName = ({ name }) => {
        const form = this.props.form.toJS();
        form.name = name;
        this.props.saveForm(form.id, form);
    };

    addField = () => {
        const form = this.props.form.toJS();
        const fieldNames = form.fields.map(field => field.name);
        form.fields.push({
            name: generateUniqueString(fieldNames, 'Новое поле'),
            type: 'input',
            options: ['Ответ']
        });
        this.props.saveForm(form.id, form);
    };

    deleteField = index => {
        const form = this.props.form.toJS();
        form.fields.splice(index, 1);
        this.props.saveForm(form.id, form);
    };

    moveField = (direction, index) => {
        if (index === 0 && direction === 'up') {
            return;
        }
        const form = this.props.form.toJS();
        if (index === form.fields.length && direction === 'down') {
            return;
        }
        let insertIndex = index;
        if (direction === 'up') {
            insertIndex--;
        } else if (direction === 'down') {
            insertIndex++;
        }
        const field = form.fields.splice(index, 1)[0];
        form.fields.splice(insertIndex, 0, field);
        this.props.saveForm(form.id, form);
    };

    renderFields = () =>
        this.props.form
            .get('fields')
            .toJS()
            .map((field, index) => (
                <EditableField
                    key={index}
                    index={index}
                    field={field}
                    form={this.props.form}
                    onSave={this.handleFieldChange}
                    onDelete={this.deleteField}
                    onMove={this.moveField}
                />
            ));

    render() {
        if (!Map.isMap(this.props.form)) {
            return null;
        }
        return (
            <div className="formFieldsWrapper">
                <EditableField
                    typeLocked
                    moveDisabled
                    disableRequired
                    deletable={false}
                    field={{ name: this.props.form.get('name') }}
                    nameLabel="Название формы"
                    onSave={this.changeFieldName}
                />
                {this.renderFields()}
                <div className="flexCenteredContent addFieldButton">
                    <Button
                        floating
                        primary
                        tooltipLabel="Добавить поле"
                        tooltipPosition="top"
                        onClick={this.addField}>
                        add
                    </Button>
                </div>
            </div>
        );
    }
}

FormFields.propTypes = {
    form: PropTypes.instanceOf(Map),
    saveForm: PropTypes.func.isRequired
};

export default FormFields;
