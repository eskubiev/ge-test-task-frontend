import { makeSimpleApiRequest } from '../../lib/redux';
import { go } from '../app/routes';

export const GET_FORMS_REQUEST = 'GET_FORMS_REQUEST';
export const GET_FORMS_SUCCESS = 'GET_FORMS_SUCCESS';
export const GET_FORMS_FAILURE = 'GET_FORMS_FAILURE';

export const GET_FORM_REQUEST = 'GET_FORM_REQUEST';
export const GET_FORM_SUCCESS = 'GET_FORM_SUCCESS';
export const GET_FORM_FAILURE = 'GET_FORM_FAILURE';

export const CREATE_FORM_REQUEST = 'CREATE_FORM_REQUEST';
export const CREATE_FORM_SUCCESS = 'CREATE_FORM_SUCCESS';
export const CREATE_FORM_FAILURE = 'CREATE_FORM_FAILURE';

export const UPDATE_FORM_REQUEST = 'UPDATE_FORM_REQUEST';
export const UPDATE_FORM_SUCCESS = 'UPDATE_FORM_SUCCESS';
export const UPDATE_FORM_FAILURE = 'UPDATE_FORM_FAILURE';

export const DELETE_FORM_REQUEST = 'DELETE_FORM_REQUEST';
export const DELETE_FORM_SUCCESS = 'DELETE_FORM_SUCCESS';
export const DELETE_FORM_FAILURE = 'DELETE_FORM_FAILURE';

export const getForms = () => {
    return dispatch =>
        makeSimpleApiRequest(
            dispatch,
            '/api/forms',
            { timeout: 500 },
            'GET_FORMS'
        );
};

export const getForm = id => {
    return dispatch =>
        makeSimpleApiRequest(
            dispatch,
            `/api/form/${id}`,
            { timeout: 500 },
            'GET_FORM'
        );
};

export const createForm = payload => {
    return dispatch =>
        makeSimpleApiRequest(
            dispatch,
            '/api/form',
            { method: 'POST', body: payload },
            'CREATE_FORM',
            response => go.toFormEdit(response.id)
        );
};

export const updateForm = (id, payload) => {
    return dispatch =>
        makeSimpleApiRequest(
            dispatch,
            `/api/form/${id}`,
            { method: 'PUT', body: payload, timeout: 300 },
            'UPDATE_FORM'
        );
};

export const deleteForm = id => {
    return dispatch =>
        makeSimpleApiRequest(
            dispatch,
            `/api/form/${id}`,
            { method: 'DELETE' },
            'DELETE_FORM',
            go.toAdminApp
        );
};
