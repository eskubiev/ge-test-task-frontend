import * as Actions from './actions';
import { Map, fromJS } from 'immutable';
import { convertListToMap } from '../../lib/immutable';

const initialState = Map({
    pending: false,
    items: Map()
});

export default (state = initialState, action) => {
    switch (action.type) {
        case Actions.GET_FORMS_SUCCESS:
            return state
                .set('pending', false)
                .set('items', convertListToMap(action.response, 'id'));
        case Actions.GET_FORM_SUCCESS:
            return state
                .set('pending', false)
                .setIn(['items', action.response.id], fromJS(action.response));
        case Actions.CREATE_FORM_SUCCESS:
            return state
                .set('pending', false)
                .setIn(['items', action.response.id], fromJS(action.response));
        case Actions.UPDATE_FORM_SUCCESS:
            return state
                .set('pending', false)
                .setIn(['items', action.response.id], fromJS(action.response));
        case Actions.DELETE_FORM_SUCCESS:
            return state
                .set('pending', false)
                .deleteIn(['items', action.response.id]);
        case Actions.GET_FORMS_REQUEST:
        case Actions.GET_FORM_REQUEST:
        case Actions.CREATE_FORM_REQUEST:
        case Actions.UPDATE_FORM_REQUEST:
        case Actions.DELETE_FORM_REQUEST:
            return state.set('pending', true);
        case Actions.GET_FORMS_FAILURE:
        case Actions.GET_FORM_FAILURE:
        case Actions.CREATE_FORM_FAILURE:
        case Actions.UPDATE_FORM_FAILURE:
        case Actions.DELETE_FORM_FAILURE:
            return state.set('pending', false);
        default:
            return state;
    }
};
