import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import { List, Button } from 'react-md';
import { FormListPreview } from './';
import { PendingOverlay } from '../progress-indicators';

import { getForms, createForm } from './actions';
import './styles.scss';

class FormsList extends Component {
    componentDidMount() {
        this.props.getForms();
    }

    addForm = () => this.props.createForm({ name: 'Новая форма' });

    renderPreviews = () =>
        this.props.forms
            .get('items')
            .map((form, id) => (
                <FormListPreview
                    key={id}
                    id={id}
                    primary={form.get('primary')}
                    name={form.get('name')}
                    createdAt={form.get('createdAt')}
                    numberOfFields={form.get('fields').count()}
                />
            ))
            .toArray();

    render() {
        return (
            <Fragment>
                <div className="formsViewsHeader">
                    <h3>Всего форм: {this.props.forms.get('items').count()}</h3>
                    <Button
                        raised
                        primary
                        disabled={this.props.forms.get('pending')}
                        iconBefore={false}
                        iconChildren="add"
                        onClick={this.addForm}>
                        Создать
                    </Button>
                </div>
                <List>{this.renderPreviews()}</List>
                {this.props.forms.get('pending') && <PendingOverlay />}
            </Fragment>
        );
    }
}

const mapStateToProps = ({ forms }) => ({
    forms
});

const mapDispatchToProps = {
    getForms,
    createForm
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FormsList);
