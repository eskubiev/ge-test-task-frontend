import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Map } from 'immutable';

import { Button, SelectionControl } from 'react-md';
import { PendingOverlay } from '../progress-indicators';
import { FormFields } from './';

import { getForm, updateForm, deleteForm } from './actions';

import './styles.scss';
import { go } from '../app/routes';

class FormEdit extends Component {
    componentDidMount() {
        const form = this.props.forms
            .get('items')
            .find(form => form.get('id') === this.props.match.params.formId);
        if (!form) {
            this.props.getForm(this.props.match.params.formId);
        }
    }

    toggleFormAsPrimary = (value, form) => {
        if (
            value &&
            window.confirm(
                'Эта форма будет назначена основной вместо выбранной ранее. Продолжить?'
            )
        ) {
            this.props.updateForm(form.get('id'), {
                ...form.toJS(),
                primary: value
            });
        } else {
            alert(
                'Выберите другую форму в качестве основной, чтобы убрать выбор с этой'
            );
        }
    };

    deleteForm = () => {
        if (window.confirm('Уверены? Это действие необратимо.')) {
            this.props.deleteForm(this.props.match.params.formId);
        }
    };

    render() {
        const form = this.props.forms.getIn([
            'items',
            this.props.match.params.formId
        ]);
        if (!Map.isMap(form)) {
            return this.props.forms.get('pending') ? <PendingOverlay /> : null;
        }
        return (
            <Fragment>
                <div className="formsViewsHeader">
                    <SelectionControl
                        id="primary-form-switch"
                        type="switch"
                        disabled={this.props.forms.get('pending')}
                        label="Основная форма"
                        name="primary-form-switch"
                        checked={form.get('primary')}
                        onChange={value =>
                            this.toggleFormAsPrimary(value, form)
                        }
                    />
                    <div className="buttonsGroup">
                        <Button
                            raised
                            disabled={this.props.forms.get('pending')}
                            iconBefore={false}
                            iconChildren="clear"
                            onClick={this.deleteForm}>
                            Удалить
                        </Button>
                        <Button
                            raised
                            primary
                            disabled={this.props.forms.get('pending')}
                            onClick={() => go.toFormPreview(form.get('id'))}>
                            Предпросмотр
                        </Button>
                    </div>
                </div>
                <FormFields form={form} saveForm={this.props.updateForm} />
                {this.props.forms.get('pending') && <PendingOverlay />}
            </Fragment>
        );
    }
}

const mapStateToProps = ({ forms }) => ({
    forms
});

const mapDispatchToProps = {
    getForm,
    updateForm,
    deleteForm
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FormEdit);
