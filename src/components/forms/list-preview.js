import React from 'react';
import PropTypes from 'prop-types';
import { ListItem, FontIcon, Avatar } from 'react-md';
import { go } from '../app/routes';
import './styles.scss';
import { dateTimeFormatOptions } from '../../lib/datetime';

const FormListPreview = ({ primary, id, name, createdAt, numberOfFields }) => {
    const date = new Date(createdAt);
    const createdAtHumanized = date.toLocaleString(
        'ru-RU',
        dateTimeFormatOptions
    );
    return (
        <ListItem
            leftAvatar={
                <Avatar
                    suffix={primary ? 'deep-purple' : ''}
                    icon={
                        <FontIcon>
                            {primary ? 'assignment_turned_in' : 'assignment'}
                        </FontIcon>
                    }
                />
            }
            rightIcon={<FontIcon>edit</FontIcon>}
            primaryText={name}
            secondaryText={`Создано: ${createdAtHumanized}\nПолей: ${numberOfFields}`}
            threeLines
            className="formPreview"
            onClick={() => go.toFormEdit(id)}
        />
    );
};

FormListPreview.propTypes = {
    primary: PropTypes.bool,
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    numberOfFields: PropTypes.number.isRequired
};

export default FormListPreview;
