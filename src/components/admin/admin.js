import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router-dom';

import { Paper, Toolbar, Button } from 'react-md';
import { FormsList, FormEdit, FormPreview } from '../forms';
import { PendingOverlay } from '../progress-indicators';

import { page404Path, go } from '../app/routes';
import { signOut } from '../profile/actions';

class AdminApp extends Component {
    render() {
        const {
            match: { path }
        } = this.props;
        return (
            <Fragment>
                <Toolbar
                    colored
                    title={
                        <span className="clickable" onClick={go.toAdminApp}>
                            Привет, Админ!
                        </span>
                    }
                    className="toolbar"
                    actions={
                        <Button
                            icon
                            disabled={this.props.profile.get('pending')}
                            tooltipLabel="Выйти из системы"
                            tooltipPosition="left"
                            onClick={this.props.signOut}>
                            exit_to_app
                        </Button>
                    }
                />
                <div className="containerWrapper">
                    <Paper zDepth={1} className="container">
                        <Switch>
                            <Route exact path={path} component={FormsList} />
                            <Route
                                exact
                                path={`${path}/form/:formId`}
                                component={FormEdit}
                            />
                            <Route
                                path={`${path}/form/:formId/preview`}
                                component={FormPreview}
                            />
                            <Redirect exact from="form" to={path} />
                            <Redirect to={`${path}${page404Path}`} />
                        </Switch>
                    </Paper>
                </div>
                {this.props.profile.get('pending') && <PendingOverlay />}
            </Fragment>
        );
    }
}

const mapStateToProps = ({ profile }) => ({
    profile
});

const mapDispatchToProps = {
    signOut
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AdminApp);
