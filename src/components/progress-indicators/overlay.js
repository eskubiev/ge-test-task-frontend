import React from 'react';
import { CircularProgress } from 'react-md';
import './styles.scss';

export default () => (
    <div className="pendingOverlay">
        <CircularProgress
            id="pending-indicator"
            className="pendingIndicator"
            scale={1.5}
        />
    </div>
);
