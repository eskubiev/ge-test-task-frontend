import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isFunciton from 'lodash/isFunction';
import isEqual from 'lodash/isEqual';

import { TextField, Button } from 'react-md';

import { generateUniqueString } from '../../lib/text';
import { getValidationErrors } from '../../lib/validation';
import messages from '../../lib/messages';

class EditableFieldOptions extends Component {
    state = { optionsValues: {}, validationErrors: {} };

    componentDidMount() {
        this.resetState();
    }

    componentDidUpdate(prevProps) {
        if (!isEqual(this.props, prevProps)) {
            this.resetState();
        }
    }

    resetState = () => {
        let optionsValues = {};
        if (Array.isArray(this.props.field.options)) {
            this.props.field.options.forEach((option, index) => {
                optionsValues[index] = option;
            });
        }
        this.setState({ optionsValues, validationErrors: {} });
    };

    add = () => {
        const optionsValues = { ...this.state.optionsValues };
        const existingOptions = Object.values(optionsValues);
        const insertIndex = existingOptions.length + 1;
        optionsValues[insertIndex] = generateUniqueString(
            existingOptions,
            'Ответ'
        );
        this.setState({ optionsValues }, this.save);
    };

    change = (value, index) =>
        this.setState({
            optionsValues: { ...this.state.optionsValues, [index]: value }
        });

    validate = () => {
        const validation = {};
        const values = Object.values(this.state.optionsValues);
        let uniqueValues = [];
        values.forEach((value, index) => {
            if (!uniqueValues.includes(value)) {
                uniqueValues.push(value);
            } else {
                const firstInstanceIndex = values.findIndex(v => v === value);
                validation[index] = [messages.validation.uniqueness];
                validation[firstInstanceIndex] = [
                    messages.validation.uniqueness
                ];
            }
        });
        return validation;
    };

    save = () => {
        const field = { ...this.props.field };
        const validationErrors = this.validate();
        if (Object.keys(validationErrors).length > 0) {
            this.setState({ validationErrors });
        } else {
            field.options = Object.values(this.state.optionsValues);
            if (
                isFunciton(this.props.onChange) &&
                JSON.stringify(field.options) !==
                    JSON.stringify(this.props.field.options)
            ) {
                this.props.onChange(field);
            }
            this.resetState();
        }
    };

    delete = index => {
        if (window.confirm('Уверены? Это действие необратимо')) {
            const optionsValues = { ...this.state.optionsValues };
            delete optionsValues[index];
            this.setState({ optionsValues }, this.save);
        }
    };

    handleKeyPress = event => {
        if (event.key === 'Enter') {
            this.save();
        }
    };

    renderOptions = () =>
        Array.isArray(this.props.field.options)
            ? this.props.field.options.map((option, index) => (
                  <div key={`${option}-${index}`} className="option">
                      <TextField
                          id={option}
                          label={`Вариант ответа ${index + 1}`}
                          value={this.state.optionsValues[index] || ''}
                          onChange={value => this.change(value, index)}
                          onBlur={this.save}
                          error={Boolean(this.state.validationErrors[index])}
                          errorText={getValidationErrors(
                              this.state.validationErrors,
                              index
                          )}
                      />
                      {this.props.field.options.length > 1 && (
                          <Button
                              icon
                              tooltipLabel="Удалить"
                              onClick={() => this.delete(index)}>
                              delete
                          </Button>
                      )}
                  </div>
              ))
            : null;

    render() {
        const hasValidationErrors =
            Object.keys(this.state.validationErrors).length > 0;
        return (
            <div
                className={`editableFieldOptions ${
                    hasValidationErrors ? 'withErrors' : ''
                }`}
                onKeyPress={this.handleKeyPress}>
                <h4>Добавьте варианты ответов</h4>
                {this.renderOptions()}
                <Button icon tooltipLabel="Добавить вариант" onClick={this.add}>
                    add
                </Button>
            </div>
        );
    }
}

EditableFieldOptions.propTypes = {
    field: PropTypes.shape({
        type: PropTypes.string.isRequired,
        options: PropTypes.arrayOf(PropTypes.string)
    }).isRequired,
    onChange: PropTypes.func
};

export default EditableFieldOptions;
