import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import isFunction from 'lodash/isFunction';

import {
    TextField,
    FileUpload,
    SelectField,
    SelectionControlGroup
} from 'react-md';

const getLabel = (field, index) => `${index + 1}. ${field.name}`;

const renderError = error => (
    <div className="md-text-field-message-container md-full-width md-text--error">
        <div className="md-text-field-message md-text-field-message--active">
            {error}
        </div>
    </div>
);

const Field = ({ field, index, value = '', onChange, error, errorText }) => {
    const props = {
        id: `${field.name}-${index}`,
        label: getLabel(field, index),
        className: 'formInput',
        value,
        onChange: isFunction(onChange) ? onChange : null
    };
    switch (field.type) {
        case 'input':
            return <TextField {...props} error={error} errorText={errorText} />;
        case 'textarea':
            return (
                <TextField
                    {...props}
                    rows={2}
                    error={error}
                    errorText={errorText}
                />
            );
        case 'checkbox':
            return (
                <Fragment>
                    <SelectionControlGroup
                        {...props}
                        className={`formControlGroup ${
                            error ? 'hasErrors' : ''
                        }`}
                        name={field.name}
                        type="checkbox"
                        controls={field.options.map(option => ({
                            label: option,
                            value: option
                        }))}
                    />
                    {error && renderError(errorText)}
                </Fragment>
            );
        case 'radio':
            return (
                <Fragment>
                    <SelectionControlGroup
                        {...props}
                        className={`formControlGroup ${
                            error ? 'hasErrors' : ''
                        }`}
                        name={field.name}
                        type="radio"
                        defaultValue={field.options[0]}
                        controls={field.options.map(option => ({
                            label: option,
                            value: option
                        }))}
                    />
                    {error && renderError(errorText)}
                </Fragment>
            );
        case 'select':
            return (
                <SelectField
                    {...props}
                    fullWidth
                    menuItems={field.options}
                    error={error}
                    errorText={errorText}
                />
            );
        case 'file':
            delete props.onChange;
            delete props.value;
            return (
                <Fragment>
                    <div
                        className={`formFileUploadGroup ${
                            error ? 'hasErrors' : ''
                        }`}>
                        <h4 className="md-subheading-1">
                            {getLabel(field, index)}
                        </h4>
                        <FileUpload
                            {...props}
                            iconBefore
                            onLoad={file =>
                                isFunction(onChange)
                                    ? onChange(file.name)
                                    : null
                            }
                            label="Выберите файл"
                        />
                        {value !== '' && (
                            <TextField
                                readOnly
                                id="uploaded-file-name"
                                defaultValue={value}
                            />
                        )}
                    </div>
                    {error && renderError(errorText)}
                </Fragment>
            );
        default:
            return null;
    }
};

Field.propTypes = {
    field: PropTypes.shape({
        name: PropTypes.string.isRequired,
        type: PropTypes.string
    }).isRequired,
    index: PropTypes.number.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    onChange: PropTypes.func,
    error: PropTypes.bool,
    errorText: PropTypes.string
};

export default Field;
