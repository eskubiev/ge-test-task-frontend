import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import isFunction from 'lodash/isFunction';
import isEqual from 'lodash/isEqual';
import validate from 'validate.js';
import { Map } from 'immutable';

import { Button, TextField, SelectField, Checkbox } from 'react-md';
import { EditableFieldOptions } from './';

import './styles.scss';
import messages from '../../lib/messages';
import { getValidationErrors, uniquenessValidator } from '../../lib/validation';

validate.validators.uniqueness = uniquenessValidator;
const constraints = {
    name: {
        presence: {
            message: messages.validation.presence,
            allowEmpty: false
        },
        uniqueness: {}
    }
};

const fieldTypes = ['input', 'textarea', 'checkbox', 'radio', 'select', 'file'];
const fieldsWithOptions = ['checkbox', 'radio', 'select'];

class EditableField extends Component {
    state = { name: '', type: '', validationErrors: {} };
    type: {
        presence: {
            message: 'Test 2',
            allowEmpty: false
        }
    };

    componentDidMount() {
        this.resetState();
    }

    componentDidUpdate(prevProps) {
        if (!isEqual(this.props, prevProps)) {
            this.resetState();
        }
    }

    resetState = () => {
        this.setState({
            editingName: false,
            touched: false,
            name: this.props.field.name,
            type: this.props.field.type,
            required: this.props.field.required,
            options: this.props.field.options,
            validationErrors: {}
        });
    };

    changeName = value => this.setState({ name: value, touched: true });

    changeType = value =>
        this.setState({ type: value, touched: true }, this.save);

    changeRequired = value =>
        this.setState({ required: value, touched: true }, this.save);

    changeOptions = ({ options }) =>
        this.setState({ options, touched: true }, this.save);

    prepareConstraints = () => {
        let fields = this.props.form.get('fields');
        if (!fields) {
            return constraints;
        }
        let comparisonFields = [];
        fields.forEach((field, key) => {
            if (key !== this.props.index) {
                comparisonFields.push(field.get('name'));
            }
        });
        const preparedConstraints = { ...constraints };
        preparedConstraints.name.uniqueness.compareWith = comparisonFields;
        return constraints;
    };

    save = () => {
        if (this.state.touched && isFunction(this.props.onSave)) {
            const field = {
                name: this.state.name,
                type: this.state.type,
                required: this.state.required,
                options: this.state.options
            };
            const validationErrors = validate(
                field,
                this.prepareConstraints(),
                {
                    fullMessages: false
                }
            );
            if (!validationErrors) {
                this.props.onSave(field, this.props.index);
                this.resetState();
            } else {
                this.setState({ validationErrors });
            }
        }
    };

    delete = () => {
        if (
            this.props.deletable &&
            isFunction(this.props.onDelete) &&
            window.confirm('Уверены? Действие необратимо')
        ) {
            this.props.onDelete(this.props.index);
        }
    };

    move = direction => {
        if (isFunction(this.props.onMove)) {
            this.props.onMove(direction, this.props.index);
        }
    };

    renderTypeSelector = () =>
        this.props.typeLocked ? null : (
            <SelectField
                fullWidth
                id="field-type-selector"
                label="Тип поля"
                menuItems={fieldTypes}
                value={this.state.type}
                onChange={this.changeType}
            />
        );

    renderMoveButtons = () => {
        const { form } = this.props;
        const totalFields = form.get('fields') ? form.get('fields').count() : 0;
        return !this.props.moveDisabled && totalFields > 0 ? (
            <Fragment>
                {this.props.index > 0 && (
                    <Button
                        icon
                        onClick={() => this.move('up')}
                        tooltipLabel="Поднять выше">
                        expand_less
                    </Button>
                )}
                {this.props.index + 1 < totalFields && (
                    <Button
                        icon
                        onClick={() => this.move('down')}
                        tooltipLabel="Опустить ниже">
                        expand_more
                    </Button>
                )}
            </Fragment>
        ) : null;
    };

    renderRequiredCheckbox = () =>
        this.props.disableRequired ? null : (
            <Checkbox
                labelBefore
                id={`required-field-checkbox-${this.props.index}`}
                name="required-field-checkbox"
                label="Обязательно"
                checked={this.state.required}
                onChange={this.changeRequired}
            />
        );

    renderDeleteButton = () =>
        this.props.deletable ? (
            <Button icon onClick={this.delete} tooltipLabel="Удалить">
                delete
            </Button>
        ) : null;

    handleKeyPress = event => {
        if (event.key === 'Enter' && this.state.touched) {
            this.save();
        }
    };

    renderOptions = () => {
        if (fieldsWithOptions.includes(this.props.field.type)) {
            return (
                <EditableFieldOptions
                    field={this.props.field}
                    onChange={this.changeOptions}
                />
            );
        }
    };

    render() {
        const hasValidationErrors =
            Object.keys(this.state.validationErrors).length > 0;
        return (
            <div
                className={`formField ${
                    hasValidationErrors ? 'withErrors' : ''
                }`}
                onKeyPress={this.handleKeyPress}>
                <div className="buttonsGroup editableFieldControls">
                    {!isNaN(this.props.index) && (
                        <span className="index">{this.props.index + 1}</span>
                    )}
                    {this.renderRequiredCheckbox()}
                    {this.renderMoveButtons()}
                    {this.renderDeleteButton()}
                </div>
                <TextField
                    id="field-name"
                    label={this.props.nameLabel}
                    value={this.state.name}
                    onChange={this.changeName}
                    onBlur={
                        this.props.field.name !== this.state.name
                            ? this.save
                            : null
                    }
                    error={Boolean(this.state.validationErrors.name)}
                    errorText={getValidationErrors(
                        this.state.validationErrors,
                        'name'
                    )}
                />
                {this.renderTypeSelector()}
                {this.renderOptions()}
            </div>
        );
    }
}

EditableField.propTypes = {
    index: PropTypes.number,
    field: PropTypes.shape({
        name: PropTypes.string,
        type: PropTypes.string,
        required: PropTypes.bool,
        options: PropTypes.arrayOf(PropTypes.string)
    }).isRequired,
    form: PropTypes.instanceOf(Map).isRequired,
    nameLabel: PropTypes.string,
    onSave: PropTypes.func,
    onDelete: PropTypes.func,
    typeLocked: PropTypes.bool,
    deletable: PropTypes.bool,
    moveDisabled: PropTypes.bool,
    onMove: PropTypes.func,
    disableRequired: PropTypes.bool
};

EditableField.defaultProps = {
    field: { name: '' },
    form: Map(),
    nameLabel: 'Название поля',
    type: 'input',
    deletable: true
};

export default EditableField;
