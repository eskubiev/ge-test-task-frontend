import { loadFromLocalStorage, saveToLocalStorage } from '../lib/local-storage';
import { generateUuid } from '../lib/crypto';

export const getForms = () => {
    const forms = loadFromLocalStorage('forms');
    if (!Array.isArray(forms)) {
        return [];
    }
    return forms;
};

export const postForm = form => {
    let forms = getForms();
    const newForm = {
        id: generateUuid(),
        createdAt: new Date(),
        fields: [],
        ...form
    };
    forms.push(newForm);
    saveToLocalStorage('forms', forms);
    return newForm;
};

export const getForm = id => {
    return getForms().find(form => form.id === id);
};

export const putForm = (id, form) => {
    let forms = getForms();
    const index = forms.findIndex(form => form.id === id);
    if (form.primary && !forms[index].primary) {
        forms.forEach((form, index, arr) => {
            arr[index] = { ...form, primary: false };
        });
    }
    forms[index] = form;
    saveToLocalStorage('forms', forms);
    return form;
};

export const deleteForm = id => {
    let forms = [];
    let deletedForm;
    getForms().forEach(form => {
        if (form.id === id) {
            deletedForm = form;
        } else {
            forms.push(form);
        }
    });
    saveToLocalStorage('forms', forms);
    return deletedForm;
};
