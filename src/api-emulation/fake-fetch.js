import { getForms, getForm, postForm, putForm, deleteForm } from './forms-api';
import isFunction from 'lodash/isFunction';

const defaultOptions = {
    method: 'GET',
    timeout: 1000
};

const getIdFromUrl = url => url.substring(url.lastIndexOf('/') + 1);

const getFormsResult = (url, options) => {
    if (url === '/api/forms' && options.method === 'GET') {
        return getForms;
    } else {
        switch (options.method) {
            case 'GET':
                return () => getForm(getIdFromUrl(url));
            case 'POST':
                return () => postForm(options.body);
            case 'PUT':
                return () => putForm(getIdFromUrl(url), options.body);
            case 'DELETE':
                return () => deleteForm(getIdFromUrl(url));
            default:
                return null;
        }
    }
};

const getPrimaryFormId = () => {
    const forms = getForms();
    const primaryForm = forms.find(form => form.primary);
    if (primaryForm) {
        return primaryForm.id;
    }
    return null;
};

const getUserResult = (url, options) => {
    if (url === '/api/user/signup' && options.method === 'POST') {
        return () => ({ role: 'client', primaryForm: getPrimaryFormId() });
    }
    if (url === '/api/user' && options.method === 'POST') {
        return () => ({ role: 'admin', primaryForm: getPrimaryFormId() });
    }
};

export const fakeFetch = (url = '', options) => {
    options = { ...defaultOptions, ...options };
    let result;
    if (url.includes('/api/user')) {
        result = getUserResult(url, options);
    }
    if (url.includes('/api/form')) {
        result = getFormsResult(url, options);
    }
    return new Promise((resolve, reject) => {
        if (isFunction(result)) {
            setTimeout(() => {
                resolve(result());
            }, options.timeout);
        }
    });
};
