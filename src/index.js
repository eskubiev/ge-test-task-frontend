import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import WebFontLoader from 'webfontloader';
import throttle from 'lodash/throttle';

import { App } from './components/app';
import configureStore from './store/configure-store';
import { saveState } from './lib/local-storage';

import './styles/index.scss';

WebFontLoader.load({
    google: {
        families: ['Roboto:300,400,500,700', 'Material Icons']
    }
});

const store = configureStore();

store.subscribe(
    throttle(() => {
        saveState({
            profile: store.getState().profile.set('pending', false)
        });
    }),
    1000
);

const renderApp = () =>
    render(
        <Provider store={store}>
            <App />
        </Provider>,
        document.getElementById('root')
    );

if (process.env.NODE_ENV !== 'production' && module.hot) {
    module.hot.accept('./components/app', () => {
        renderApp();
    });
}

renderApp();
